/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.nursery.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.nursery.parent.Owner;
import org.springframework.samples.nursery.parent.OwnerRepository;
import org.springframework.samples.nursery.parent.Child;
import org.springframework.samples.nursery.parent.ChildRepository;
import org.springframework.samples.nursery.parent.ChildGender;
import org.springframework.samples.nursery.teacher.Teacher;
import org.springframework.samples.nursery.teacher.TeacherRepository;
import org.springframework.samples.nursery.attend.Attend;
import org.springframework.samples.nursery.attend.AttendenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration test of the Service and the Repository layer.
 * <p>
 * ClinicServiceSpringDataJpaTests subclasses benefit from the following services provided
 * by the Spring TestContext Framework:
 * </p>
 * <ul>
 * <li><strong>Spring IoC container caching</strong> which spares us unnecessary set up
 * time between test execution.</li>
 * <li><strong>Dependency Injection</strong> of test fixture instances, meaning that we
 * don't need to perform application context lookups. See the use of
 * {@link Autowired @Autowired} on the <code>{@link
 * ClinicServiceTests#clinicService clinicService}</code> instance variable, which uses
 * autowiring <em>by type</em>.
 * <li><strong>Transaction management</strong>, meaning each test method is executed in
 * its own transaction, which is automatically rolled back by default. Thus, even if tests
 * insert or otherwise change database state, there is no need for a teardown or cleanup
 * script.
 * <li>An {@link org.springframework.context.ApplicationContext ApplicationContext} is
 * also inherited and can be used for explicit bean lookup if necessary.</li>
 * </ul>
 *
 * @author Ken Krebs
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Michael Isvy
 * @author Dave Syer
 */
@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
class ClinicServiceTests {

	@Autowired
	protected OwnerRepository owners;

	@Autowired
	protected ChildRepository pets;

	@Autowired
	protected AttendenceRepository visits;

	@Autowired
	protected TeacherRepository vets;

	@Test
	void shouldFindOwnersByLastName() {
		Collection<Owner> owners = this.owners.findByLastName("Davis");
		assertThat(owners).hasSize(2);

		owners = this.owners.findByLastName("Daviss");
		assertThat(owners).isEmpty();
	}

	@Test
	void shouldFindSingleOwnerWithPet() {
		Owner owner = this.owners.findById(1);
		assertThat(owner.getLastName()).startsWith("Franklin");
		assertThat(owner.getPets()).hasSize(1);
		assertThat(owner.getPets().get(0).getType()).isNotNull();
		assertThat(owner.getPets().get(0).getType().getName()).isEqualTo("cat");
	}

	@Test
	@Transactional
	void shouldInsertOwner() {
		Collection<Owner> owners = this.owners.findByLastName("Schultz");
		int found = owners.size();

		Owner owner = new Owner();
		owner.setFirstName("Sam");
		owner.setLastName("Schultz");
		owner.setAddress("4, Evans Street");
		owner.setCity("Wollongong");
		owner.setTelephone("4444444444");
		this.owners.save(owner);
		assertThat(owner.getId().longValue()).isNotEqualTo(0);

		owners = this.owners.findByLastName("Schultz");
		assertThat(owners.size()).isEqualTo(found + 1);
	}

	@Test
	@Transactional
	void shouldUpdateOwner() {
		Owner owner = this.owners.findById(1);
		String oldLastName = owner.getLastName();
		String newLastName = oldLastName + "X";

		owner.setLastName(newLastName);
		this.owners.save(owner);

		// retrieving new name from database
		owner = this.owners.findById(1);
		assertThat(owner.getLastName()).isEqualTo(newLastName);
	}

	@Test
	void shouldFindPetWithCorrectId() {
		Child pet7 = this.pets.findById(7);
		assertThat(pet7.getName()).startsWith("Samantha");
		assertThat(pet7.getOwner().getFirstName()).isEqualTo("Jean");

	}

	@Test
	void shouldFindAllPetTypes() {
		Collection<ChildGender> petTypes = this.pets.findPetTypes();

		ChildGender petType1 = EntityUtils.getById(petTypes, ChildGender.class, 1);
		assertThat(petType1.getName()).isEqualTo("cat");
		ChildGender petType4 = EntityUtils.getById(petTypes, ChildGender.class, 4);
		assertThat(petType4.getName()).isEqualTo("snake");
	}

	@Test
	@Transactional
	void shouldInsertPetIntoDatabaseAndGenerateId() {
		Owner owner6 = this.owners.findById(6);
		int found = owner6.getPets().size();

		Child pet = new Child();
		pet.setName("bowser");
		Collection<ChildGender> types = this.pets.findPetTypes();
		pet.setType(EntityUtils.getById(types, ChildGender.class, 2));
		pet.setBirthDate(LocalDate.now());
		owner6.addPet(pet);
		assertThat(owner6.getPets().size()).isEqualTo(found + 1);

		this.pets.save(pet);
		this.owners.save(owner6);

		owner6 = this.owners.findById(6);
		assertThat(owner6.getPets().size()).isEqualTo(found + 1);
		// checks that id has been generated
		assertThat(pet.getId()).isNotNull();
	}

	@Test
	@Transactional
	void shouldUpdatePetName() throws Exception {
		Child pet7 = this.pets.findById(7);
		String oldName = pet7.getName();

		String newName = oldName + "X";
		pet7.setName(newName);
		this.pets.save(pet7);

		pet7 = this.pets.findById(7);
		assertThat(pet7.getName()).isEqualTo(newName);
	}

	@Test
	void shouldFindVets() {
		Collection<Teacher> vets = this.vets.findAll();

		Teacher vet = EntityUtils.getById(vets, Teacher.class, 3);
		assertThat(vet.getLastName()).isEqualTo("Douglas");
		assertThat(vet.getNrOfSpecialties()).isEqualTo(2);
		assertThat(vet.getSpecialties().get(0).getName()).isEqualTo("dentistry");
		assertThat(vet.getSpecialties().get(1).getName()).isEqualTo("surgery");
	}

	@Test
	@Transactional
	void shouldAddNewVisitForPet() {
		Child pet7 = this.pets.findById(7);
		int found = pet7.getVisits().size();
		Attend visit = new Attend();
		pet7.addVisit(visit);
		visit.setDescription("test");
		this.visits.save(visit);
		this.pets.save(pet7);

		pet7 = this.pets.findById(7);
		assertThat(pet7.getVisits().size()).isEqualTo(found + 1);
		assertThat(visit.getId()).isNotNull();
	}

	@Test
	void shouldFindVisitsByPetId() throws Exception {
		Collection<Attend> visits = this.visits.findByPetId(7);
		assertThat(visits).hasSize(2);
		Attend[] visitArr = visits.toArray(new Attend[visits.size()]);
		assertThat(visitArr[0].getDate()).isNotNull();
		assertThat(visitArr[0].getPetId()).isEqualTo(7);
	}

}
